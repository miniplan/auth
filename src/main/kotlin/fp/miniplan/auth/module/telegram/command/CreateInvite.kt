package fp.miniplan.auth.module.telegram.command

import com.pengrad.telegrambot.TelegramBot
import com.pengrad.telegrambot.model.User
import fe.tgbotutil.buildMarkdown
import fe.tgbotutil.command.Command
import fe.tgbotutil.command.CommandArgumentCreator
import fe.tgbotutil.command.EmptyArgument
import fe.tgbotutil.ext.sendMarkdownV2
import fe.tgbotutil.ext.sendText
import fp.miniplan.auth.config.ExternalConfig
import fp.miniplan.auth.ext.transactionHandleException
import fp.miniplan.auth.ext.transactionRaw
import fp.miniplan.auth.module.database.UserInvite
import fp.miniplan.auth.module.telegram.command.base.LockedCommand
import fp.miniplan.auth.util.TokenHandler
import org.eclipse.jetty.util.HttpCookieStore.Empty
import org.jetbrains.exposed.sql.Database
import org.koin.core.component.KoinComponent
import org.koin.core.component.inject
import java.util.*

data class MinistrantArgument(val uuid: String?) {
    companion object Creator : CommandArgumentCreator<MinistrantArgument> {
        override fun parseArgs(args: List<String>) = MinistrantArgument(args.singleOrNull())
        override fun requiredArgs() = arrayOf(0, 1)
    }
}

object CreateInvite : LockedCommand<MinistrantArgument>(
    "/invite", "[ministrant_id]", "Create an invite", MinistrantArgument.Creator
), KoinComponent {
    private val database by inject<Database>()
    private val externalConfig by inject<ExternalConfig>()

    override fun execWithWhitelist(args: MinistrantArgument, bot: TelegramBot, user: User, chatId: Long) {
        val token = TokenHandler.getStandardCharsetString(32)
        val response = database.transactionHandleException {
            UserInvite.new {
                this.createdAt = System.currentTimeMillis()
                this.token = token
                this.ministrantPreSelect = args.uuid?.let { UUID.fromString(it) }
            }
        }

        if (response != null) {
            bot.sendMarkdownV2(chatId, buildMarkdown {
                text("Invite created:").space().code(externalConfig.inviteLink?.format(token) ?: token)
            })
        } else {
            bot.sendText(chatId, "Something went wrong while creating the invite!")
        }
    }
}
