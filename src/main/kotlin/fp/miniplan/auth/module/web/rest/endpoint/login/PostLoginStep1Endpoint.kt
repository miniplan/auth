package fp.miniplan.auth.module.web.rest.endpoint.login

import fp.miniplan.auth.module.database.AccountState
import fp.miniplan.auth.module.web.WebRole
import fp.miniplan.auth.module.web.rest.base.rest.ApiRestEndpoint
import fp.miniplan.auth.module.web.rest.base.rest.RestApiVersion
import fp.miniplan.auth.module.web.rest.endpoint.TwilioMixin
import fp.miniplan.auth.module.web.rest.endpoint.UserMixin
import fp.miniplan.auth.util.sanitizeNumber
import io.javalin.http.Context
import io.javalin.http.HandlerType
import org.eclipse.jetty.http.HttpStatus
import org.koin.core.component.KoinComponent

object PostLoginStep1Endpoint : ApiRestEndpoint<LoginBody>(
    mapOf(RestApiVersion.V1 to LoginBody::class.java),
    "login",
    roles = arrayOf(WebRole.Default),
    method = HandlerType.POST
), KoinComponent {

    override fun handle(ctx: Context, body: LoginBody) {
        val sanitizedNumber = sanitizeNumber(body.phoneNumber)

        val user = UserMixin.handle(ctx, sanitizedNumber)
        if (user?.state != AccountState.Done) {
            ctx.status(HttpStatus.BAD_REQUEST_400).result("no_such_user")
            return
        }

        TwilioMixin.handle(ctx, user.phoneNumber)
    }
}

data class LoginBody(
    val phoneNumber: String
)
