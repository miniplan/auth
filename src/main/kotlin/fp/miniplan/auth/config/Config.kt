package fp.miniplan.auth.config

import fe.koinhelper.sqlite.config.SqliteConfig
import fe.koinhelper.uptime.config.UptimeTrackerConfig

data class Config(
    val runtimeConfig: RuntimeConfig,
    val twilioConfig: TwilioConfig,
    val smsConfig: SmsConfig,
    val telegramConfig: TelegramConfig,
    val telegramWhitelistConfig: TelegramWhitelistConfig,
    val redisConfig: RedisConfig,
    val sqliteConfig: SqliteConfig,
    val webConfig: WebConfig,
    val hasuraConfig: HasuraConfig,
    val jwtConfig: JwtConfig,
    val externalConfig: ExternalConfig,
    val uptimeTrackerConfig: UptimeTrackerConfig,
    val gPlayDemoConfig: GPlayDemoConfig,
)

data class RuntimeConfig(
    val debug: Boolean,
    val appName: String,
    val disableSms: Boolean = false,
    val codeBypass: String? = null,
)

data class WebConfig(
    val host: String? = null,
    val port: Int = 2898,
)

data class HasuraConfig(
    val host: String,
    val port: Int? = 8080,
    val tls: Boolean = false,
    val adminPassword: String,
)

data class TwilioConfig(
    val accountSID: String,
    val authToken: String,
    val serviceSID: String
)

data class SmsConfig(
    val loginMessage: String,
)

data class JwtConfig(
    val jwtAccessTokenSecret: String,
    val accessTokenExpirySeconds: Long,
    val jwtRefreshTokenSecret: String,
    val refreshTokenExpirySeconds: Long,
    val jwtClaimNamespace: String,
)

data class TelegramConfig(
    val botToken: String,
)

data class TelegramWhitelistConfig(
    val user: Long,
)

data class RedisConfig(
    val host: String,
    val port: Int,
    val password: String? = null
)

data class ExternalConfig(
    val inviteLink: String? = null,
)

data class GPlayDemoConfig(
    val enabled: Boolean = false,
    val phoneNumber: String = "+4312345678901",
    val code: String = "123456"
)
