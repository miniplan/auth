package fp.miniplan.auth.module.web

import com.auth0.jwt.exceptions.JWTVerificationException
import com.auth0.jwt.exceptions.TokenExpiredException
import fp.miniplan.auth.config.JwtConfig
import fp.miniplan.auth.config.RuntimeConfig
import fp.miniplan.auth.config.WebConfig
import fp.miniplan.auth.ext.getJwt
import fp.miniplan.auth.module.web.rest.endpoint.*
import fp.miniplan.auth.module.web.rest.endpoint.jwt.GetRefreshEndpoint
import fp.miniplan.auth.module.web.rest.endpoint.login.PostLoginStep1Endpoint
import fp.miniplan.auth.module.web.rest.endpoint.login.PostLoginStep2Endpoint
import fp.miniplan.auth.module.web.rest.endpoint.signup.PostSignupStep1Endpoint
import fp.miniplan.auth.module.web.rest.endpoint.signup.PostSignupStep2Endpoint
import io.javalin.Javalin
import io.javalin.http.Context
import javalinjwt.JavalinJWT
import org.eclipse.jetty.http.HttpStatus
import org.koin.dsl.module
import java.time.LocalDateTime


val webServerModule = module {
    single {
        val config = get<WebConfig>()
        val jwtConfig = get<JwtConfig>()
        val runtimeConfig = get<RuntimeConfig>()
        val jwtHandler = get<JwtHandler>()

        val app = Javalin.create { cfg ->
            cfg.showJavalinBanner = false
            cfg.requestLogger.http { ctx, executionTimeMs ->
                println("%s: %s %s -> %s (took %.2fms)".format(
                    LocalDateTime.now().toString(),
                    ctx.method(),
                    ctx.path(),
                    ctx.status(),
                    executionTimeMs
                ))
            }
            cfg.accessManager(
                JwtAccessManager(
                    runtimeConfig.appName,
                    jwtHandler,
                    WebRole.Default
                )
            )
        }

        val jwtBeforeHandler: (Context) -> Unit = { context ->
            context.getJwt()?.let {
                try {
                    val jwt = jwtHandler.verify(jwtHandler.accessType, it)
                    JavalinJWT.addDecodedToContext(context, jwt)
                } catch (ex: TokenExpiredException) {
                    context.attribute(JwtAccessManager.CONTEXT_ATTRIBUTE, JwtAccessManager.JwtStatus.JwtExpired)
                } catch (_: JWTVerificationException) {
                    context.attribute(JwtAccessManager.CONTEXT_ATTRIBUTE, JwtAccessManager.JwtStatus.JwtError)
                }
            }
        }

        app.before("app/*", jwtBeforeHandler)

        app.routes {
            GetRefreshEndpoint
            PostLoginStep1Endpoint
            PostLoginStep2Endpoint
            PostSignupStep1Endpoint
            PostSignupStep2Endpoint
            GetCheckTokenEndpoint
            GetJWTFromRedisEndpoint
        }

        if (runtimeConfig.debug) {
            app.options("*", { it.status(HttpStatus.OK_200) }, WebRole.Default)
            app.before { ctx ->
                ctx.header("Access-Control-Max-Age", "86400")
                ctx.header("Access-Control-Allow-Origin", "*")
                ctx.header("Access-Control-Allow-Headers", "Authorization,Content-Type,rest-api-version")
                ctx.header("Access-Control-Allow-Credentials", "true")
                ctx.header("Vary", "Origin")
            }
        }

        app
    }
}

