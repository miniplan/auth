package fp.miniplan.auth.module.web.rest.endpoint.signup

import fe.logger.Logger
import fp.miniplan.auth.ext.globalPhoneUtil
import fp.miniplan.auth.ext.parseOrNull
import fp.miniplan.auth.ext.transactionHandleException
import fp.miniplan.auth.module.database.AuthUser
import fp.miniplan.auth.module.database.AuthUsers
import fp.miniplan.auth.module.database.UserInvite
import fp.miniplan.auth.module.database.UserInvites
import fp.miniplan.auth.module.web.WebRole
import fp.miniplan.auth.module.web.rest.base.rest.ApiRestEndpoint
import fp.miniplan.auth.module.web.rest.base.rest.RestApiVersion
import fp.miniplan.auth.module.web.rest.endpoint.TwilioMixin
import io.javalin.http.Context
import io.javalin.http.HandlerType
import org.eclipse.jetty.http.HttpStatus
import org.jetbrains.exposed.sql.Database
import org.koin.core.component.KoinComponent
import org.koin.core.component.inject

object PostSignupStep1Endpoint : ApiRestEndpoint<SignupData>(
    mapOf(RestApiVersion.V1 to SignupData::class.java),
    "signup",
    roles = arrayOf(WebRole.Default),
    method = HandlerType.POST
), KoinComponent {
    private val database by inject<Database>()
    private val logger = Logger(PostSignupStep1Endpoint::class.java)

    override fun handle(ctx: Context, body: SignupData) {
        val parsedNumber = globalPhoneUtil.parseOrNull(body.phoneNumber) ?: run {
            ctx.status(HttpStatus.BAD_REQUEST_400).result("phone_number_parse_failed")
            return
        }

        val invite = database.transactionHandleException {
            UserInvite.find {
                (UserInvites.token eq body.inviteToken)
            }.firstOrNull()
        }

        if (invite == null) {
            ctx.status(HttpStatus.BAD_REQUEST_400).result("invalid_token")
            return
        }

        if (invite.signedUpAt != null || invite.phoneVerifiedAt != null) {
            ctx.status(HttpStatus.BAD_REQUEST_400).result("token_has_already_been_used")
            return
        }

        val user = database.transactionHandleException {
            AuthUser.find {
                AuthUsers.phoneNumber eq parsedNumber
            }.firstOrNull()
        }

        if(user != null){
            ctx.status(HttpStatus.BAD_REQUEST_400).result("phone_number_already_registered")
            return
        }

        TwilioMixin.handle(ctx, parsedNumber)
    }
}

data class SignupData(
    val inviteToken: String,
    val phoneNumber: String
)
