package fp.miniplan.auth.module.web.rest.endpoint.signup

import com.pengrad.telegrambot.TelegramBot
import fe.tgbotutil.buildMarkdown
import fe.tgbotutil.config.TelegramConfig
import fe.tgbotutil.ext.sendMarkdownV2
import fp.miniplan.auth.config.TelegramWhitelistConfig
import fp.miniplan.auth.ext.globalPhoneUtil
import fp.miniplan.auth.ext.parseOrNull
import fp.miniplan.auth.ext.transactionHandleException
import fp.miniplan.auth.module.database.AccountState
import fp.miniplan.auth.module.database.AuthUser
import fp.miniplan.auth.module.database.UserInvite
import fp.miniplan.auth.module.database.UserInvites
import fp.miniplan.auth.module.hasura.HasuraConnector
import fp.miniplan.auth.module.web.JwtHandler
import fp.miniplan.auth.module.web.WebRole
import fp.miniplan.auth.module.web.rest.base.rest.ApiRestEndpoint
import fp.miniplan.auth.module.web.rest.base.rest.RestApiVersion
import fp.miniplan.auth.module.web.rest.endpoint.TokenResponse
import fp.miniplan.auth.module.web.rest.endpoint.TokenState
import fp.miniplan.auth.module.web.rest.endpoint.TwilioMixin
import fp.miniplan.auth.module.web.rest.endpoint.UserInfo
import fp.miniplan.auth.util.TokenHandler
import io.javalin.http.Context
import io.javalin.http.HandlerType
import io.javalin.json.jsonMapper
import org.eclipse.jetty.http.HttpStatus
import org.jetbrains.exposed.sql.Database
import org.koin.core.component.KoinComponent
import org.koin.core.component.inject
import redis.clients.jedis.JedisPooled

object PostSignupStep2Endpoint : ApiRestEndpoint<ConfirmSignup>(
    mapOf(RestApiVersion.V1 to ConfirmSignup::class.java),
    "signup/confirm",
    roles = arrayOf(WebRole.Default),
    method = HandlerType.POST
), KoinComponent {
    private val database by inject<Database>()
    private val redis by inject<JedisPooled>()
    private val jwtHandler by inject<JwtHandler>()
    private val hasura by inject<HasuraConnector>()
    private val telegramBot by inject<TelegramBot>()
    private val telegramWhitelistConfig by inject<TelegramWhitelistConfig>()

    override fun handle(ctx: Context, body: ConfirmSignup) {
        val invite = database.transactionHandleException {
            UserInvite.find {
                (UserInvites.token eq body.invite)
            }.firstOrNull()
        }

        if (invite == null) {
            ctx.status(HttpStatus.BAD_REQUEST_400).result("invalid_token")
            return
        }

        val phoneNumber = globalPhoneUtil.parseOrNull(body.phoneNumber) ?: run {
            ctx.status(HttpStatus.BAD_REQUEST_400).result("phone_number_parse_failed")
            return
        }

        if (!redis.exists(TwilioMixin.redisSmsCodeKey(phoneNumber, body.otp.trim()))) {
            ctx.status(HttpStatus.BAD_REQUEST_400).result("invalid_otp")
            return
        }

        val user = database.transactionHandleException(printStackTrace = false) {
            AuthUser.new {
                this.firstname = body.firstname.trim()
                this.lastname = body.lastname.trim()
                this.phoneNumber = phoneNumber
                this.invitedVia = invite.id
            }
        }

        if (user == null) {
            //unique constraint on number failed, did user try to signup twice?
            ctx.status(HttpStatus.BAD_REQUEST_400).result("phone_number_already_registered")
            return
        }

        val insertMinistrant = database.transactionHandleException {
            user.state = AccountState.Done
            invite.phoneVerifiedAt = System.currentTimeMillis()

            invite.ministrantPreSelect
        }

        hasura.createUser(user.id.value, insertMinistrant)


        telegramBot.sendMarkdownV2(telegramWhitelistConfig.user, buildMarkdown {
            spaced(
                { text("User") },
                { code("${body.firstname} ${body.lastname}") },
                { text("just signed up") }
            )
        })

        val (accessToken, refreshToken) = jwtHandler.createBothTokens(user.id.value, "miniplan")
        redis.set(refreshToken, TokenState.Generated.name)


        val token = "token_${TokenHandler.getStandardCharsetString(64)}"
        redis.set(
            token,
            ctx.jsonMapper().toJsonString(
                TokenResponse(
                    accessToken,
                    refreshToken,
                    UserInfo(user.firstname, user.lastname, user.phoneNumber, user.id.value)
                ), TokenResponse::class.java
            )
        )

        ctx.status(HttpStatus.OK_200).json(mapOf("key" to token))
    }
}

data class ConfirmSignup(
    val phoneNumber: String,
    val invite: String,
    val firstname: String,
    val lastname: String,
    val otp: String
)
