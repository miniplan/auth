package fp.miniplan.auth.module.web.rest.endpoint

enum class TokenState {
    Generated, Invalidated
}
