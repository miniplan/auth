package fp.miniplan.auth.module.web.rest.base.rest

import fp.miniplan.auth.ext.transactionHandleException
import fp.miniplan.auth.module.database.AuthUser
import fp.miniplan.auth.module.web.JwtHandler
import fp.miniplan.auth.module.web.WebRole
import io.javalin.http.Context
import io.javalin.http.HandlerType
import org.jetbrains.exposed.sql.Database
import org.koin.core.component.KoinComponent
import org.koin.core.component.inject
import java.util.*

abstract class ApiUserRestEndpoint<T>(
    private val classes: Map<RestApiVersion, Class<out T>>,
    vararg path: String,
    roles: Array<WebRole>,
    method: HandlerType
) : ApiRestEndpoint<T>(classes, *path, roles = roles.plus(WebRole.User), method = method), KoinComponent {
    protected val database by inject<Database>()

    override fun handle(ctx: Context, body: T) {
        ctx.getJwtUser(database)?.let { this.handle(ctx, it, body) }
    }


    abstract fun handle(ctx: Context, user: AuthUser, body: T)
}

fun Context.getJwtUser(database: Database): AuthUser? {
    val userId = this.attribute<String>(JwtHandler.JWT_CLAIM_USER_ID)!!
    return database.transactionHandleException { AuthUser.findById(UUID.fromString(userId)) }
}
