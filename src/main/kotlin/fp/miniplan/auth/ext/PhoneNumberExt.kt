package fp.miniplan.auth.ext

import com.google.i18n.phonenumbers.NumberParseException
import com.google.i18n.phonenumbers.PhoneNumberUtil
import com.google.i18n.phonenumbers.Phonenumber

fun Phonenumber.PhoneNumber.toDbFormat() = "+${this.countryCode}${this.nationalNumber}"

val globalPhoneUtil: PhoneNumberUtil = PhoneNumberUtil.getInstance()

fun PhoneNumberUtil.parseOrNull(phoneNumber: String, country: String = "AT"): String? {
    return try {
        this.parse(phoneNumber, country).toDbFormat()
    } catch (e: NumberParseException) {
        e.printStackTrace()
        null
    }
}
