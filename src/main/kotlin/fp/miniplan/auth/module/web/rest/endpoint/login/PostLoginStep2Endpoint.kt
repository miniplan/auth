package fp.miniplan.auth.module.web.rest.endpoint.login

import fp.miniplan.auth.module.web.JwtHandler
import fp.miniplan.auth.module.web.WebRole
import fp.miniplan.auth.module.web.rest.base.rest.ApiRestEndpoint
import fp.miniplan.auth.module.web.rest.base.rest.RestApiVersion
import fp.miniplan.auth.module.web.rest.endpoint.*
import fp.miniplan.auth.util.sanitizeNumber
import io.javalin.http.Context
import io.javalin.http.HandlerType
import org.eclipse.jetty.http.HttpStatus
import org.koin.core.component.KoinComponent
import org.koin.core.component.inject
import redis.clients.jedis.JedisPooled

object PostLoginStep2Endpoint : ApiRestEndpoint<ConfirmLogin>(
    mapOf(RestApiVersion.V1 to ConfirmLogin::class.java),
    "login/confirm",
    roles = arrayOf(WebRole.Default),
    method = HandlerType.POST
), KoinComponent {
    private val redis by inject<JedisPooled>()
    private val jwtHandler by inject<JwtHandler>()

    override fun handle(ctx: Context, body: ConfirmLogin) {
        val sanitizedNumber = sanitizeNumber(body.phoneNumber)

        val user = UserMixin.handle(ctx, sanitizedNumber) ?: run {
            ctx.status(HttpStatus.BAD_REQUEST_400).result("no_such_user")
            return
        }

        if (!redis.exists(TwilioMixin.redisSmsCodeKey(user.phoneNumber, body.otp))) {
            ctx.status(HttpStatus.BAD_REQUEST_400).result("invalid_otp")
            return
        }

        val (accessToken, refreshToken) = jwtHandler.createBothTokens(user.id.value, "miniplan")
        redis.set(refreshToken, TokenState.Generated.name)

        val tokenResponse = TokenResponse(
            accessToken,
            refreshToken,
            UserInfo(user.firstname, user.lastname, user.phoneNumber, user.id.value)
        )

        ctx.status(HttpStatus.OK_200).json(tokenResponse)
    }
}


data class ConfirmLogin(
    val phoneNumber: String,
    val otp: String
)
