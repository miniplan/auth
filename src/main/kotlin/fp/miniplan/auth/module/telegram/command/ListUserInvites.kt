package fp.miniplan.auth.module.telegram.command

import com.pengrad.telegrambot.TelegramBot
import com.pengrad.telegrambot.model.User
import com.pengrad.telegrambot.model.request.ParseMode
import com.pengrad.telegrambot.request.SendMessage
import fe.markdown.tablegen.Column
import fe.markdown.tablegen.Row
import fe.tgbotutil.command.Command
import fe.tgbotutil.command.EmptyArgument
import fe.tgbotutil.ext.sendMessage
import fp.miniplan.auth.ext.transactionRaw
import fp.miniplan.auth.module.database.AuthUser
import fp.miniplan.auth.module.database.UserInvite
import fp.miniplan.auth.module.database.UserInvites
import fp.miniplan.auth.module.telegram.command.base.LockedCommand
import org.jetbrains.exposed.sql.Database
import org.koin.core.component.KoinComponent
import org.koin.core.component.inject

object ListUserInvites : LockedCommand<EmptyArgument>(
    "/listuserinvites", null, "List all existing UserInvites", EmptyArgument
), KoinComponent, TableCommand<UserInvite> {
    private val database by inject<Database>()

    override val header = Row(
        Column("Token"),
        Column("Created at"),
        Column("Signed up at"),
        Column("Phone verified at"),
        Column("Ministrant")
    )

    override fun generateRow(obj: UserInvite) = Row(
        Column(obj.token),
        Column(obj.createdAt.toString()),
        Column(obj.signedUpAt.toString()),
        Column(obj.phoneVerifiedAt.toString()),
        Column(obj.ministrantPreSelect.toString())
    )


    override fun execWithWhitelist(args: EmptyArgument, bot: TelegramBot, user: User, chatId: Long) {
        this.sendTable(bot, chatId, database.transactionRaw { UserInvite.all().toList() })
    }
}
