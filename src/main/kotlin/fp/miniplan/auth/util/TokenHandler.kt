package fp.miniplan.auth.util

import java.security.SecureRandom

object TokenHandler {
    private val secureRandom = SecureRandom()
    private const val stdChars = "ABCDEFGHIJKLMNOPQRSTUVWXYZabcdefghijklmnopqrstuvwxyz0123456789"

    fun getStandardCharsetString(length: Int): String {
        return buildString {
            repeat(length) {
                this.append(stdChars[secureRandom.nextInt(stdChars.length)])
            }
        }
    }

    fun getDigitString(length: Int, min: Int = 0, max: Int = 9): String {
        return buildString {
            repeat(length) {
                this.append(min + secureRandom.nextInt(max - min + 1))
            }
        }
    }
}
