package fp.miniplan.auth.module.telegram

import com.pengrad.telegrambot.TelegramBot
import fp.miniplan.auth.config.TelegramConfig
import org.koin.dsl.module

val telegramBotModule = module {
    single {
        val telegramConfig = get<TelegramConfig>()

        TelegramBot(telegramConfig.botToken)
    }
}
