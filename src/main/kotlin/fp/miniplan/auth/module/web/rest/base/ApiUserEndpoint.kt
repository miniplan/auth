package fp.miniplan.auth.module.web.rest.base

import fe.javalin.rest.ApiEndpoint
import fp.miniplan.auth.ext.transactionHandleException
import fp.miniplan.auth.module.database.AuthUser
import fp.miniplan.auth.module.web.JwtHandler
import fp.miniplan.auth.module.web.WebRole
import fp.miniplan.auth.module.web.rest.base.rest.getJwtUser
import io.javalin.http.Context
import io.javalin.http.HandlerType
import org.jetbrains.exposed.sql.Database
import org.koin.core.component.KoinComponent
import org.koin.core.component.inject
import java.util.UUID

abstract class ApiUserEndpoint(vararg paths: String, roles: Array<WebRole>, method: HandlerType = HandlerType.GET) :
    ApiEndpoint(
        *paths, roles = roles.plus(WebRole.User), method = method
    ), KoinComponent {
    protected val database by inject<Database>()

    override fun handle(ctx: Context) {
        //JwtAccessManager won't forward us routes which have not claim set, so we can safely ignore "force access" it
        ctx.getJwtUser(database)?.let {
            this.handle(ctx, it)
        }
    }

    abstract fun handle(ctx: Context, user: AuthUser)
}

fun Context.getJwtUser(database: Database): AuthUser? {
    val userId = this.attribute<String>(JwtHandler.JWT_CLAIM_USER_ID)!!
    return database.transactionHandleException { AuthUser.findById(UUID.fromString(userId)) }
}
