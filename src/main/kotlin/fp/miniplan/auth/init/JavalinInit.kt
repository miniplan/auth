package fp.miniplan.auth.init

import fe.koinhelper.ext.KoinInit
import fp.miniplan.auth.config.WebConfig
import io.javalin.Javalin
import org.koin.core.component.inject

object JavalinInit : KoinInit {
    private val javalin by inject<Javalin>()
    private val webConfig by inject<WebConfig>()

    override fun init() {
        javalin.start(webConfig.host, webConfig.port)
    }
}
