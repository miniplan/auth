package fp.miniplan.auth.module.telegram.command

import com.pengrad.telegrambot.TelegramBot
import com.pengrad.telegrambot.model.request.ParseMode
import com.pengrad.telegrambot.request.SendMessage
import fe.markdown.tablegen.Row
import fe.markdown.tablegen.TableGenerator

interface TableCommand<T> {
    val header: Row

    fun generateRow(obj: T): Row

    fun sendTable(bot: TelegramBot, chatId: Long, rows: List<T>) {
        bot.execute(SendMessage(chatId, tableGenerator.generate(header, rows.map { generateRow(it) })).apply {
            this.parseMode(ParseMode.MarkdownV2)
        })
    }

    companion object {
        val tableGenerator = TableGenerator()
    }
}
