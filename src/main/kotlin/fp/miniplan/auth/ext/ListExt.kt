package fp.miniplan.auth.ext

fun <T> List<T>.toPair() = this[0] to this[1]

operator fun <T> List<T>.component6() = this[5]

inline fun <T> List<T>.getMakeSureOrElse(index: Int, makeSure: (T) -> Boolean, defaultValue: (Int) -> T): T {
    return if (index in 0..lastIndex) {
        val value = get(index)
        if (makeSure(value)) return value else defaultValue(index)
    } else defaultValue(index)
}

fun <T> List<T>.getReverse(idx: Int): T {
    return this[this.size - 1 - idx]
}

fun <T> Array<T>.loop(hook: (T, first: Boolean, last: Boolean) -> Unit) = this.iterator().loop(this.size, hook)

fun <T> Iterator<T>.loop(size: Int, hook: (T, first: Boolean, last: Boolean) -> Unit) {
    val it = this.iterator()
    var i = 0
    while (it.hasNext()) {
        val t = it.next()
        hook(t, i == 0, i++ == size - 1)
    }
}
