package fp.miniplan.auth.ext

import java.util.*

fun String?.toUUIDOrNull() = if (this != null) UUID.fromString(this) else null
