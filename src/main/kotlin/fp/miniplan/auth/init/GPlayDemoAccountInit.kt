package fp.miniplan.auth.init

import fe.koinhelper.ext.KoinInit
import fp.miniplan.auth.config.GPlayDemoConfig
import fp.miniplan.auth.ext.transactionHandleException
import fp.miniplan.auth.module.database.AccountState
import fp.miniplan.auth.module.database.AuthUser
import fp.miniplan.auth.module.database.AuthUsers
import fp.miniplan.auth.module.hasura.HasuraConnector
import org.jetbrains.exposed.sql.Database
import org.jetbrains.exposed.sql.SqlExpressionBuilder.eq
import org.koin.core.component.getScopeName
import org.koin.core.component.inject

object GPlayDemoAccountInit : KoinInit {
    private val gPlayDemoConfig by inject<GPlayDemoConfig>()
    private val database by inject<Database>()
    private val hasura by inject<HasuraConnector>()

    override fun init() {
        val authUser = database.transactionHandleException {
            AuthUser.find(AuthUsers.phoneNumber eq gPlayDemoConfig.phoneNumber).firstOrNull()
        }

        if (authUser == null && gPlayDemoConfig.enabled) {
            val user = database.transactionHandleException {
                AuthUser.new {
                    this.firstname = "Gplay"
                    this.lastname = "Demo"
                    this.state = AccountState.Done
                    this.phoneNumber = gPlayDemoConfig.phoneNumber
                }
            }

            if(user != null){
                hasura.createUser(user.id.value, null)
            }
        } else if (authUser != null && !gPlayDemoConfig.enabled) {
            database.transactionHandleException {
                authUser.delete()
            }

            hasura.deleteUser(authUser.id.value.toString())
        }
    }
}
