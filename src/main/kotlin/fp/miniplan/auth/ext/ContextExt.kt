package fp.miniplan.auth.ext

import io.javalin.http.Context

fun Context.getJwt() = this.header("Authorization")?.let { header ->
    val split = header.split(" ")
    if (split.size != 2 || split[0] != "Bearer") null else split[1]
}
