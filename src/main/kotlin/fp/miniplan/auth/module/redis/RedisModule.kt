package fp.miniplan.auth.module.redis

import fp.miniplan.auth.config.RedisConfig
import org.koin.dsl.module
import redis.clients.jedis.CommandArguments
import redis.clients.jedis.JedisPooled
import redis.clients.jedis.Protocol.Command
import redis.clients.jedis.commands.ProtocolCommand

val redisModule = module {
    single {
        val cfg = get<RedisConfig>()
        JedisPooled(cfg.host, cfg.port, null,  if(cfg.password.isNullOrEmpty()) null else cfg.password)
    }
}
