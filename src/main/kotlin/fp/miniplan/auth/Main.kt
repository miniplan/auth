package fp.miniplan.auth

import com.google.gson.Gson
import fe.gson.extensions.fileToInstance
import fe.koinhelper.config.configModules
import fe.koinhelper.ext.init
import fe.koinhelper.ext.serviceChecker
import fe.koinhelper.sqlite.module.sqlite.sqliteModule
import fe.koinhelper.uptime.init.UptimeHeartBeatInit
import fe.koinhelper.uptime.module.service.defaultScheduledExecutorModule
import fe.koinhelper.uptime.module.uptime.uptimeHeartbeatModule
import fe.logger.Logger
import fe.version.helper.Version.Companion.readVersion
import fp.miniplan.auth.config.Config
import fp.miniplan.auth.init.DatabaseInit
import fp.miniplan.auth.init.GPlayDemoAccountInit
import fp.miniplan.auth.init.JavalinInit
import fp.miniplan.auth.init.TelegramBotInit
import fp.miniplan.auth.module.hasura.hasuraModule
import fp.miniplan.auth.module.redis.redisModule
import fp.miniplan.auth.module.request.requestModule
import fp.miniplan.auth.module.telegram.telegramBotModule
import fp.miniplan.auth.module.telegram.telegramUpdateHandlerModule
import fp.miniplan.auth.module.twilio.twilioModule
import fp.miniplan.auth.module.web.jwtHandlerModule
import fp.miniplan.auth.module.web.webServerModule
import fp.miniplan.auth.service.RedisServiceChecker
import org.koin.core.context.startKoin

val gson = Gson()
val logger = Logger("Main")

fun main(args: Array<String>) {
    if (args.isEmpty()) error("Please provide path to config!")
    val version = readVersion()
    logger.info("Launching version %s", version.full)

    startKoin {
        modules(
            configModules(
                gson.fileToInstance<Config>(args[0]), mapOf(
                    "config" to { it },
                    "runtimeConfig" to { it.runtimeConfig },
                    "twilioConfig" to { it.twilioConfig },
                    "smsConfig" to { it.smsConfig },
                    "telegramConfig" to { it.telegramConfig },
                    "telegramWhitelistConfig" to { it.telegramWhitelistConfig },
                    "uptimeTrackerConfig" to { it.uptimeTrackerConfig },
                    "redisConfig" to { it.redisConfig },
                    "webConfig" to { it.webConfig },
                    "sqliteConfig" to { it.sqliteConfig },
                    "hasuraConfig" to { it.hasuraConfig },
                    "jwtConfig" to { it.jwtConfig },
                    "externalConfig" to { it.externalConfig },
                    "gPlayDemoConfig" to { it.gPlayDemoConfig }
                )
            )
        )
        modules(
            twilioModule,
            jwtHandlerModule,
            sqliteModule,
            redisModule,
            hasuraModule,
            telegramBotModule,
            telegramUpdateHandlerModule,
            webServerModule,
            requestModule,
            uptimeHeartbeatModule,
            defaultScheduledExecutorModule
        )

        serviceChecker(RedisServiceChecker)
        init(DatabaseInit, JavalinInit, TelegramBotInit, UptimeHeartBeatInit, GPlayDemoAccountInit)
    }
}


