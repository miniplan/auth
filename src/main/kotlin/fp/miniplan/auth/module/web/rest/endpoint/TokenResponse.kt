package fp.miniplan.auth.module.web.rest.endpoint

import java.util.*

data class TokenResponse(
    val accessToken: String,
    val refreshToken: String,
    val userInfo: UserInfo
)

data class UserInfo(
    val firstname: String,
    val lastname: String,
    val phoneNumber: String,
    val userId: UUID,
)
