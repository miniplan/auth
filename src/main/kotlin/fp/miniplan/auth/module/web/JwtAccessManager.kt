package fp.miniplan.auth.module.web

import com.auth0.jwt.interfaces.DecodedJWT
import io.javalin.http.Context
import io.javalin.http.Handler
import io.javalin.security.AccessManager
import io.javalin.security.RouteRole
import org.eclipse.jetty.http.HttpStatus

class JwtAccessManager(
    private val appName: String,
    private val jwtHandler: JwtHandler,
    private val defaultRole: RouteRole
) : AccessManager {

    companion object {
        const val CONTEXT_ATTRIBUTE = "jwt"
    }

    @Throws(Exception::class)
    override fun manage(handler: Handler, ctx: Context, routeRoles: Set<RouteRole>) {
        if (routeRoles.contains(WebRole.Default)) {
            handler.handle(ctx)
            return
        }

        val jwt = ctx.attribute<Any>(CONTEXT_ATTRIBUTE)
        if (jwt is JwtStatus) {
            ctx.send(Response.InvalidAuthentication(jwt.name))
        } else if (jwt is DecodedJWT) {
            try {
                val (jwtAppName, userId) = jwtHandler.extractToken(jwt)
                if (appName != jwtAppName) {
                    ctx.send(Response.InvalidAuthentication("Jwt has not been issued for this app!"))
                    return
                }

                ctx.attribute(JwtHandler.JWT_CLAIM_USER_ID, userId)

                handler.handle(ctx)
            } catch (e: JwtHandler.MissingClaimException) {
                ctx.send(Response.WithErrorResponse(e.message ?: "Jwt error", HttpStatus.BAD_REQUEST_400, null))
                return
            }
        }
    }

    enum class JwtStatus {
        JwtError, JwtExpired;
    }
}
