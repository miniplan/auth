package fp.miniplan.auth.module.telegram.command

import com.pengrad.telegrambot.TelegramBot
import com.pengrad.telegrambot.model.User
import com.pengrad.telegrambot.model.request.ParseMode
import com.pengrad.telegrambot.request.SendMessage
import fe.markdown.tablegen.Column
import fe.markdown.tablegen.Row
import fe.tgbotutil.command.Command
import fe.tgbotutil.command.EmptyArgument
import fp.miniplan.auth.ext.transactionRaw
import fp.miniplan.auth.module.database.AuthUser
import fp.miniplan.auth.module.telegram.command.base.LockedCommand
import org.jetbrains.exposed.sql.Database
import org.koin.core.component.KoinComponent
import org.koin.core.component.inject

object ListMinistranten : LockedCommand<EmptyArgument>(
    "/listusers", null, "List all existing Ministranten", EmptyArgument
), KoinComponent, TableCommand<AuthUser> {
    private val database by inject<Database>()
    override val header =
        Row(Column("ID"), Column("Firstname"), Column("Lastname"), Column("Phone"), Column("PhoneVerified"))

    override fun generateRow(obj: AuthUser) = Row(
        Column(obj.id.value.toString()),
        Column(obj.firstname),
        Column(obj.lastname),
        Column(obj.phoneNumber),
        Column(obj.state.toString())
    )


    override fun execWithWhitelist(args: EmptyArgument, bot: TelegramBot, user: User, chatId: Long) {
        this.sendTable(bot, chatId, database.transactionRaw { AuthUser.all().toList() })
    }
}
