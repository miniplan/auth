import org.jetbrains.kotlin.gradle.tasks.KotlinCompile

plugins {
    kotlin("jvm") version "1.8.0"
    java
    application
    id("net.nemerosa.versioning") version "2.15.1"
}

group = "fp.miniplan.auth"
version = versioning.info.tag ?: versioning.info.full

repositories {
    mavenCentral()
    maven(url = "https://jitpack.io")
}

dependencies {
    implementation(kotlin("stdlib"))

    implementation("org.jetbrains.exposed:exposed-core:0.40.1")
    implementation("org.jetbrains.exposed:exposed-dao:0.40.1")
    implementation("org.jetbrains.exposed:exposed-jdbc:0.40.1")
    implementation("org.jetbrains.exposed:exposed-java-time:0.40.1")
    implementation("redis.clients:jedis:5.1.0")
    implementation("com.google.code.gson:gson:2.10")
    implementation("com.gitlab.grrfe:GSONKtExtensions:2.1.2")
    implementation("com.gitlab.grrfe:Logger:3.0.0")
    implementation("com.gitlab.grrfe:httpkt:13.0.0-alpha.25")
    implementation("com.gitlab.grrfe:koin-helper:2.x.x-SNAPSHOT")
    implementation("com.gitlab.grrfe:koin-exposed:1.x.x-SNAPSHOT")
    implementation("com.gitlab.grrfe:version-helper:2.0.6")
    implementation("com.gitlab.grrfe:sqlite-koin:1.0.2")
    implementation("com.gitlab.grrfe:hostbeat-koin:4.0.0")
    implementation("com.gitlab.grrfe:md-tablegen:1.0.4")
    implementation("com.gitlab.grrfe:javalin-rest:0.0.5")
    implementation("com.gitlab.grrfe:tgbotutilkt:1.1.3")

    implementation("com.googlecode.libphonenumber:libphonenumber:8.12.55")
    implementation("io.insert-koin:koin-core:3.2.2")
    implementation("io.javalin:javalin:5.1.3")
    implementation("com.github.kmehrunes:javalin-jwt:v0.6")
    implementation("com.fasterxml.jackson.core:jackson-databind:2.13.4.2")
    implementation("com.fasterxml.jackson.module:jackson-module-kotlin:2.13.4")
    implementation("com.fasterxml.jackson.datatype:jackson-datatype-jsr310:2.13.4")

    implementation("com.github.pengrad:java-telegram-bot-api:6.2.0")

    implementation("org.slf4j:slf4j-simple:2.0.3")


    implementation("com.google.code.gson:gson:2.10")

    implementation("org.xerial:sqlite-jdbc:3.39.3.0")
    testImplementation("io.insert-koin:koin-test:3.2.2")

    testImplementation(kotlin("test"))
}

tasks.getByName("versionFile") {
    this.setProperty("file", File("resources/main/version.properties"))
}

tasks.getByName("classes") {
    this.dependsOn.add(tasks.getByName("versionFile"))
}

tasks.getByName<Jar>("jar") {
    manifest {
        this.attributes["Main-Class"] = "fp.miniplan.auth.MainKt"
    }

    this.from(configurations.runtimeClasspath.get().map { if (it.isDirectory) it else zipTree(it) })
    this.duplicatesStrategy = DuplicatesStrategy.EXCLUDE
}

tasks.withType<Tar> {
    duplicatesStrategy = DuplicatesStrategy.EXCLUDE
}

tasks.withType<Zip>{
    duplicatesStrategy = DuplicatesStrategy.EXCLUDE
}


tasks.withType<KotlinCompile> {
    kotlinOptions.jvmTarget = "1.8"
}

