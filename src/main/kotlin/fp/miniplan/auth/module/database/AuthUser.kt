package fp.miniplan.auth.module.database

import org.jetbrains.exposed.dao.IntEntity
import org.jetbrains.exposed.dao.IntEntityClass
import org.jetbrains.exposed.dao.UUIDEntity
import org.jetbrains.exposed.dao.UUIDEntityClass
import org.jetbrains.exposed.dao.id.EntityID
import org.jetbrains.exposed.dao.id.UUIDTable
import java.util.UUID

object AuthUsers : UUIDTable("auth_users") {

    val firstname = varchar("firstname", 64)
    val lastname = varchar("lastname", 64)
    val phoneNumber = varchar("phoneNumber", 64)
    val state = enumeration("state", AccountState::class).default(AccountState.PhoneRequired)
    val invitedVia = reference("invitedVia", UserInvites).nullable()

    init {
        uniqueIndex(phoneNumber)
    }
}

enum class AccountState {
    PhoneRequired, Done
}

class AuthUser(id: EntityID<UUID>) : UUIDEntity(id) {
    companion object : UUIDEntityClass<AuthUser>(AuthUsers)

    var userId by AuthUsers.id
    var firstname by AuthUsers.firstname
    var lastname by AuthUsers.lastname
    var phoneNumber by AuthUsers.phoneNumber
    var state by AuthUsers.state
    var invitedVia by AuthUsers.invitedVia
}
