package fp.miniplan.auth.module.telegram.command.base

import com.pengrad.telegrambot.TelegramBot
import com.pengrad.telegrambot.model.User
import fe.tgbotutil.command.Command
import fe.tgbotutil.command.CommandArgumentCreator
import fe.tgbotutil.config.TelegramConfig
import fp.miniplan.auth.config.TelegramWhitelistConfig
import org.koin.core.component.KoinComponent
import org.koin.core.component.inject

abstract class LockedCommand<A>(
    name: String, usage: String?, description: String,
    creator: CommandArgumentCreator<A>,
    vararg aliases: String = emptyArray()
) : Command<A>(name, usage, description, creator, *aliases), KoinComponent {
    private val telegramConfig by inject<TelegramWhitelistConfig>()

    override fun exec(args: A, bot: TelegramBot, user: User, chatId: Long) {
        if(telegramConfig.user == user.id()){
            execWithWhitelist(args, bot, user, chatId)
        }
    }

    abstract fun execWithWhitelist (args: A, bot: TelegramBot, user: User, chatId: Long)
}
