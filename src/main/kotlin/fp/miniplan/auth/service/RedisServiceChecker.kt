package fp.miniplan.auth.service

import fe.koinhelper.ext.KoinServiceChecker
import org.koin.core.component.inject
import redis.clients.jedis.CommandArguments
import redis.clients.jedis.JedisPooled
import redis.clients.jedis.Protocol.Command

object RedisServiceChecker : KoinServiceChecker {
    private val redis by inject<JedisPooled>()

    override fun serviceAvailable(): Exception? {
        return kotlin.runCatching { redis.ping() }.exceptionOrNull() as? Exception?
    }
}
