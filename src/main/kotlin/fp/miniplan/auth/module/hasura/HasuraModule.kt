package fp.miniplan.auth.module.hasura

import com.fasterxml.jackson.databind.BeanDescription
import com.google.gson.JsonElement
import com.google.gson.JsonObject
import com.google.gson.JsonPrimitive
import fe.gson.extensions.*
import fe.httpkt.Request
import fe.httpkt.json.JsonBody
import fe.httpkt.json.readToJson
import fe.httpkt.util.readToString
import fp.miniplan.auth.config.HasuraConfig
import org.koin.dsl.module
import java.util.*

val hasuraModule = module {
    single {
        val cfg = get<HasuraConfig>()
        HasuraConnector(cfg.host, cfg.port, cfg.tls, cfg.adminPassword)
    }
}

class HasuraConnector(
    private val host: String,
    private val port: Int?,
    private val tls: Boolean,
    private val password: String
) {
    private val request = Request {
        this.addHeader("x-hasura-admin-secret", password)
        this.addHeader("content-type", "application/json")
    }

    private val baseUrl = buildString {
        this.append("http")
        if (tls) this.append("s")

        this.append("://").append(host)
        if (port != null) {
            this.append(":").append(port)
        }
    }

    class HasuraException(val name: String, response: String) : Exception(response)

    @Throws(HasuraException::class)
    fun createUser(userId: UUID, ministrant: UUID?): Boolean {
        val con =
            request.post("$baseUrl/api/rest/user", body = JsonBody(mapOf("id" to userId, "ministrant" to ministrant)))
        if (con.responseCode != 200) {
            throw HasuraException("CreateUser", con.readToString())
        }

        con.readToString()
        return con.responseCode == 200
//        {"insert_User":{"affected_rows" : 1}}
//        {"path":"$.selectionSet.insert_User.args.objects","error":"Uniqueness violation. duplicate key value violates unique constraint \"User_pkey\"","code":"constraint-violation"}
    }

    @Throws(HasuraException::class)
    fun createMinistrant(firstname: String, lastname: String): Any? {
        return insert(
            mapOf("firstname" to firstname, "lastname" to lastname),
            "CreateMinistrant",
            "insert_Ministrant",
            "ministrant"
        ) {
            UUID.fromString(it.string("id")!!)
        }
    }

    @Throws(HasuraException::class)
    fun deleteTasks(ministrant: String): Boolean {
        val con = request.delete("$baseUrl/api/rest/task/$ministrant")
        if (con.responseCode != 200) {
            throw HasuraException("DeleteTasks", con.readToString())
        }

        con.readToString()
        return con.responseCode == 200
    }

    @Throws(HasuraException::class)
    fun deleteUser(user: String): Boolean {
        val con = request.delete("$baseUrl/api/rest/user/$user")
        if (con.responseCode != 200) {
            throw HasuraException("DeleteUser", con.readToString())
        }

        return con.responseCode == 200
    }

    @Throws(HasuraException::class)
    fun deleteUserMinistrant(user: String): Boolean {
        val con = request.delete("$baseUrl/api/rest/userministrant/$user")
        if (con.responseCode != 200) {
            throw HasuraException("DeleteUserMinistrant", con.readToString())
        }

        return con.responseCode == 200
    }

    @Throws(HasuraException::class)
    fun deleteMinistrant(ministrant: String): Boolean {
        val con = request.delete("$baseUrl/api/rest/ministrant/$ministrant")
        if (con.responseCode != 200) {
            throw HasuraException("DeleteMinistrant", con.readToString())
        }

        return con.responseCode == 200
    }

    @Throws(HasuraException::class)
    fun getUserMinistranten(user: String): List<Pair<String?, String?>> {
        return get("UserMinistrant", "UserMinistrant", "userministrant/$user") {
            it.string("user") to it.string("ministrant")
        }
    }


    data class Task(
        val description: String,
        val duration: Int,
        val id: Long,
        val messe: Long,
        val ministrant: String,
        val start: String,
        val taskType: Int
    )

    @Throws(HasuraException::class)
    fun getMinistrantTasks(ministrant: String): List<Task> {
        return get("Tasks", "Task", "tasks/$ministrant") {
            Task(it.string("description")!!, it.int("duration")!!, it.long("id")!!,
                it.long("messe")!!, it.string("ministrant")!!, it.string("start")!!, it.int("task_type")!!)
        }
    }

    @Throws(HasuraException::class)
    fun getUserMinistrant(user: String): String? {
        return get("User", "User", "user/$user"){
            it.string("ministrant")
        }.firstOrNull()
    }

    private fun <R> get(name: String, arrayName: String, api: String, returnFn: (JsonObject) -> R): List<R> {
        val con = request.get("$baseUrl/api/rest/$api")
        if (con.responseCode != 200) {
            throw HasuraException(name, con.readToString())
        }

        return con.readToJson().asJsonObject.array(arrayName)?.map { it as JsonObject }?.run {
            this.map { returnFn(it) }
        } ?: throw HasuraException(name, con.readToString())
    }

    private fun <R> insert(
        item: Map<String, String>,
        name: String,
        key: String,
        api: String,
        returnFn: (JsonObject) -> R
    ): R {
        val con = request.post("$baseUrl/api/rest/$api", body = JsonBody(item))
        if (con.responseCode != 200) {
            throw HasuraException(name, con.readToString())
        }

        return con.readToJson().asJsonObject.obj(key)?.array("returning")?.firstOrNull()?.asJsonObject?.run {
            returnFn(this)
        } ?: throw HasuraException(name, con.readToString())
    }
}

