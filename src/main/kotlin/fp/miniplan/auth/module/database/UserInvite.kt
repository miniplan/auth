package fp.miniplan.auth.module.database

import org.jetbrains.exposed.dao.IntEntity
import org.jetbrains.exposed.dao.IntEntityClass
import org.jetbrains.exposed.dao.id.EntityID
import org.jetbrains.exposed.dao.id.IntIdTable

object UserInvites : IntIdTable("user_invites") {
    val token = varchar("token", 128).uniqueIndex()
    val createdAt = long("createdAt")
    val signedUpAt = long("signedUpAt").nullable()
    val phoneVerifiedAt = long("phoneVerifiedAt").nullable()
    val ministrantPreSelect = uuid("ministrantPreSelect").nullable()
}

class UserInvite(id: EntityID<Int>) : IntEntity(id) {
    companion object : IntEntityClass<UserInvite>(UserInvites)

    var token by UserInvites.token
    var createdAt by UserInvites.createdAt
    var signedUpAt by UserInvites.signedUpAt
    var phoneVerifiedAt by UserInvites.phoneVerifiedAt
    var ministrantPreSelect by UserInvites.ministrantPreSelect
}

