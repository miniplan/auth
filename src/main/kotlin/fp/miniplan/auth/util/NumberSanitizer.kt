package fp.miniplan.auth.util

fun sanitizeNumber(inputNumber: String): String {
    var number = inputNumber.replace(" ", "")
    if (number.startsWith("+43")) {
        number = number.substring(3)
    }

    if (number.startsWith("0")) {
        number = number.substring(1)
    }

    return "+43 $number"
}
