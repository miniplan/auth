package fp.miniplan.auth.ext

import redis.clients.jedis.JedisPooled
import redis.clients.jedis.UnifiedJedis
import redis.clients.jedis.params.SetParams

typealias SetParamFn = SetParams.() -> Unit

fun UnifiedJedis.set(key: String, value: String?, params: SetParamFn): String? {
    return this.set(key, value, SetParams().apply(params))
}

