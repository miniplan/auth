package fp.miniplan.auth.module.web.rest.endpoint

import fe.javalin.rest.ApiPathParam1Endpoint
import fe.javalin.rest.StringPathParamValidator
import fp.miniplan.auth.ext.transactionHandleException
import fp.miniplan.auth.module.database.UserInvites
import fp.miniplan.auth.module.web.WebRole
import io.javalin.http.Context
import io.javalin.http.HandlerType
import org.jetbrains.exposed.sql.Database
import org.jetbrains.exposed.sql.and
import org.jetbrains.exposed.sql.select
import org.koin.core.component.KoinComponent
import org.koin.core.component.inject

const val id = "{id}"

object GetCheckTokenEndpoint : ApiPathParam1Endpoint<String>(
    "check/$id",
    roles = arrayOf(WebRole.Default),
    xValidator = StringPathParamValidator(id),
    method = HandlerType.GET
), KoinComponent {
    private val sqlDatabase by inject<Database>()

    override fun handle(ctx: Context, x: String) {
        val userInvite = sqlDatabase.transactionHandleException {
            UserInvites.select {
                (UserInvites.token eq x) and (UserInvites.signedUpAt eq null) and (UserInvites.phoneVerifiedAt eq null)
            }.firstOrNull()
        }

        ctx.json(ExistsToken(userInvite != null))
    }
}

data class ExistsToken(
    val exists: Boolean,
)
