package fp.miniplan.auth.module.web

import io.javalin.security.RouteRole

enum class WebRole : RouteRole {
    Default, User, Admin, WsBypass;
}
