package fp.miniplan.auth.module.telegram.command

import com.pengrad.telegrambot.TelegramBot
import com.pengrad.telegrambot.model.User
import fe.tgbotutil.buildMarkdown
import fe.tgbotutil.buildMarkdownString
import fe.tgbotutil.command.CommandArgumentCreator
import fe.tgbotutil.ext.sendMarkdownV2
import fp.miniplan.auth.ext.transactionHandleException
import fp.miniplan.auth.ext.transactionRaw
import fp.miniplan.auth.module.database.AuthUser
import fp.miniplan.auth.module.database.UserInvite
import fp.miniplan.auth.module.database.UserInvites
import fp.miniplan.auth.module.hasura.HasuraConnector
import fp.miniplan.auth.module.telegram.command.base.LockedCommand
import org.jetbrains.exposed.sql.Database
import org.koin.core.component.KoinComponent
import org.koin.core.component.inject
import java.util.*

data class DeleteUserArgument(val uuid: String) {
    companion object Creator : CommandArgumentCreator<DeleteUserArgument> {
        override fun parseArgs(args: List<String>) = DeleteUserArgument(args.single())
        override fun requiredArgs() = arrayOf(1)
    }
}

object DeleteUser : LockedCommand<DeleteUserArgument>(
    "/delete",
    "[user_id]",
    "Deletes a user from the system",
    DeleteUserArgument.Creator
), KoinComponent {
    private val hasura by inject<HasuraConnector>()
    private val database by inject<Database>()

    override fun execWithWhitelist(args: DeleteUserArgument, bot: TelegramBot, user: User, chatId: Long) {
        val authUser = database.transactionHandleException {
            AuthUser.findById(UUID.fromString(args.uuid))
        }
        val userMinistrantDelete = hasura.deleteUserMinistrant(args.uuid)
        val userDelete = hasura.deleteUser(args.uuid)

        val (deleteTasks, deleteMinistrant) = if (authUser?.invitedVia != null) {
            val ministrant = database.transactionHandleException {
                UserInvite.findById(authUser.invitedVia!!)?.ministrantPreSelect
            }

            if (ministrant != null) {
                hasura.deleteTasks(ministrant.toString()) to hasura.deleteMinistrant(ministrant.toString())
            }
            else false to false
        } else false to false

        database.transactionRaw {
            authUser?.delete()
        }

        bot.sendMarkdownV2(chatId, buildMarkdown {
            this.text("Task delete: ").code(deleteTasks.toString())
                .text(", Ministrant delete: ").code(deleteMinistrant.toString())
                .text(", UserMinistrant delete: ").code(userMinistrantDelete.toString())
                .text(", User delete: ").code(userDelete.toString())
        })
    }
}
