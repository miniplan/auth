package fp.miniplan.auth.init

import com.pengrad.telegrambot.TelegramBot
import fe.koinhelper.ext.KoinInit
import fe.tgbotutil.TelegramUpdateHandler
import org.koin.core.component.inject

object TelegramBotInit : KoinInit {
    private val bot by inject<TelegramBot>()
    private val updateHandler by inject<TelegramUpdateHandler>()

    override fun init() {
        bot.setUpdatesListener(updateHandler)
    }
}

