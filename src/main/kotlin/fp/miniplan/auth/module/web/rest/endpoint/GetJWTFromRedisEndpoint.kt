package fp.miniplan.auth.module.web.rest.endpoint

import fe.javalin.rest.ApiPathParam1Endpoint
import fe.javalin.rest.StringPathParamValidator
import fp.miniplan.auth.module.web.WebRole
import io.javalin.http.Context
import io.javalin.http.HandlerType
import org.eclipse.jetty.http.HttpStatus
import org.koin.core.component.KoinComponent
import org.koin.core.component.inject
import redis.clients.jedis.JedisPooled

const val token = "token"

object GetJWTFromRedisEndpoint : ApiPathParam1Endpoint<String>(
    "pickupjwt/{$token}",
    roles = arrayOf(WebRole.Default),
    xValidator = StringPathParamValidator(id),
    method = HandlerType.GET
), KoinComponent {
    private val redis by inject<JedisPooled>()

    override fun handle(ctx: Context, x: String) {
        if(!x.startsWith("token_")){
            ctx.status(HttpStatus.BAD_REQUEST_400).result("token_must_start_with_token_")
            return
        }

        val value = redis.get(x)
        if(value == null){
            ctx.status(HttpStatus.NOT_FOUND_404).result("token_does_not_exist")
            return
        }

        redis.del(x)
        ctx.json(value)
    }
}
