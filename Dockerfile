FROM openjdk:slim-bullseye
ARG tag
COPY build/libs/miniplan-auth-$tag.jar /app.jar

#config.json must be mounted via volume
CMD ["java", "-jar", "app.jar", "config.json"]
