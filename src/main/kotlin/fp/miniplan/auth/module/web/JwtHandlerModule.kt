package fp.miniplan.auth.module.web

import com.auth0.jwt.JWT
import com.auth0.jwt.JWTCreator
import com.auth0.jwt.JWTVerifier
import com.auth0.jwt.algorithms.Algorithm
import com.auth0.jwt.exceptions.AlgorithmMismatchException
import com.auth0.jwt.exceptions.InvalidClaimException
import com.auth0.jwt.exceptions.SignatureVerificationException
import com.auth0.jwt.exceptions.TokenExpiredException
import com.auth0.jwt.interfaces.DecodedJWT
import fp.miniplan.auth.config.JwtConfig
import org.koin.dsl.module
import java.util.*


val jwtHandlerModule = module {
    single {
        JwtHandler(get())
    }
}

class JwtHandler(val jwtConfig: JwtConfig) {

    sealed class JwtType(secret: String, val expiryTime: Long) {
        val sign: Algorithm = Algorithm.HMAC256(secret)
        val verifier: JWTVerifier = JWT.require(sign).build()

        class AccessType(secret: String, expiryTime: Long) : JwtType(secret, expiryTime)
        class RefreshType(secret: String, expiryTime: Long) : JwtType(secret, expiryTime)
    }

    val accessType = JwtType.AccessType(jwtConfig.jwtAccessTokenSecret, jwtConfig.accessTokenExpirySeconds)
    val refreshType = JwtType.RefreshType(jwtConfig.jwtRefreshTokenSecret, jwtConfig.refreshTokenExpirySeconds)

    fun getTokenExpiry(type: JwtType): Long {
        return type.expiryTime
    }

    @Throws(
        AlgorithmMismatchException::class, SignatureVerificationException::class,
        TokenExpiredException::class, InvalidClaimException::class
    )
    fun verify(jwtType: JwtType, token: String): DecodedJWT {
        return jwtType.verifier.verify(token)
    }


    companion object {
        val JWT_CLAIM_APP_NAME = "appName"
        val JWT_CLAIM_USER_ID = "x-hasura-user-id"
        val JWT_CLAIM_ALLOWED_USER_ROLES = "x-hasura-allowed-roles"
        val JWT_CLAIM_DEFAULT_ROLE = "x-hasura-default-role"
    }

    data class JwtData(val appName: String, val userId: UUID, val allowedRoles: List<String>, val defaultRole: String)

    class MissingClaimException(text: String) : java.lang.Exception(text)

    @Throws(MissingClaimException::class)
    fun extractToken(decodedJWT: DecodedJWT): JwtData {
        val claimMap = decodedJWT.getClaim(jwtConfig.jwtClaimNamespace).asMap()

        val appName = claimMap[JWT_CLAIM_APP_NAME]?.toString()
            ?: throw MissingClaimException("No '$JWT_CLAIM_APP_NAME' claim!")

        val userId = claimMap[JWT_CLAIM_USER_ID]?.toString()?.let { UUID.fromString(it) }
            ?: throw MissingClaimException("No '$JWT_CLAIM_USER_ID' claim!")

        val allowedRoles = claimMap[JWT_CLAIM_ALLOWED_USER_ROLES] as? List<*>
            ?: throw MissingClaimException("No '$JWT_CLAIM_ALLOWED_USER_ROLES' claim!")

        val defaultRole = claimMap[JWT_CLAIM_DEFAULT_ROLE]?.toString()
            ?: throw MissingClaimException("No '$JWT_CLAIM_DEFAULT_ROLE' claim!")

        return JwtData(appName, userId, allowedRoles as List<String>, defaultRole)
    }

    private fun createToken(
        userId: UUID,
        appName: String,
        expiresInSeconds: Long,
    ): JWTCreator.Builder {
        return JWT.create()
            .withClaim(
                jwtConfig.jwtClaimNamespace, mapOf(
                    JWT_CLAIM_APP_NAME to appName,
                    JWT_CLAIM_USER_ID to userId.toString(),
                    JWT_CLAIM_ALLOWED_USER_ROLES to listOf("user"),
                    JWT_CLAIM_DEFAULT_ROLE to "user"
                )
            )
            .issuedAt()
            .expiresInSeconds(expiresInSeconds)
    }

    fun createToken(
        jwtType: JwtType,
        userId: UUID,
        appName: String,
    ): String {
        return createToken(userId, appName, jwtType.expiryTime).sign(jwtType.sign)
    }

    fun createBothTokens(
        userId: UUID,
        appName: String,
    ): Pair<String, String> {
        return createToken(accessType, userId, appName) to createToken(
            refreshType,
            userId,
            appName
        )
    }

    private fun JWTCreator.Builder.issuedAt(now: Long = System.currentTimeMillis() / 1000): JWTCreator.Builder {
        return this.withClaim("iat", now)
    }

    private fun JWTCreator.Builder.expiresInSeconds(expirySeconds: Long): JWTCreator.Builder {
        return this.withClaim("exp", (System.currentTimeMillis() / 1000) + expirySeconds)
    }
}
