package fp.miniplan.auth.module.web.rest.endpoint

import fe.logger.Logger
import fp.miniplan.auth.config.GPlayDemoConfig
import fp.miniplan.auth.config.RuntimeConfig
import fp.miniplan.auth.config.SmsConfig
import fp.miniplan.auth.ext.set
import fp.miniplan.auth.module.twilio.TwilioSender
import fp.miniplan.auth.util.TokenHandler
import io.javalin.http.Context
import org.eclipse.jetty.http.HttpStatus
import org.koin.core.component.KoinComponent
import org.koin.core.component.inject
import redis.clients.jedis.JedisPooled

object TwilioMixin : Mixin<String, Unit?>, KoinComponent {
    private val gPlayDemoConfig by inject<GPlayDemoConfig>()
    private val redis by inject<JedisPooled>()
    private val twilioSender by inject<TwilioSender>()
    private val runtimeConfig by inject<RuntimeConfig>()
    private val smsConfig by inject<SmsConfig>()

    val redisCoolDownKey: (String) -> String = { number -> "sms_cooldown_${number}" }
    val redisSmsCodeKey: (String, String) -> String = { number, code -> "sms_code_${number}_$code" }

    private const val codeExpireSeconds = 60 * 5L

    private val logger = Logger(TwilioMixin::class.java)

    override fun handle(ctx: Context, input: String): Unit? {
        val cooldownKey = redisCoolDownKey(input)
        val cooldown = redis.pexpireTime(cooldownKey)
        // -2 = no key, -1 = no expire time
        if (cooldown > 0) {
            ctx.status(HttpStatus.BAD_REQUEST_400).result("wait_before_requesting_new_code")
            return null
        }

        val code = if (gPlayDemoConfig.enabled && gPlayDemoConfig.phoneNumber == input) {
            gPlayDemoConfig.code
        } else TokenHandler.getDigitString(6)

        redis.set(cooldownKey, "1") {
            this.ex(60)
        }

        redis.set(redisSmsCodeKey(input, code), "1") {
            this.ex(codeExpireSeconds)
        }

        if (!runtimeConfig.disableSms) {
            val con = twilioSender.sendMessage(input, smsConfig.loginMessage.format(code, codeExpireSeconds / 60))
            logger.info("User $input requested a sms code")
            if (con.responseCode != 201) {
                ctx.status(HttpStatus.INTERNAL_SERVER_ERROR_500).result("code_could_not_be_sent")
                return null
            }
        }

        ctx.status(HttpStatus.OK_200).result("code_sent")
        return Unit
    }
}
