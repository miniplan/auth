package fp.miniplan.auth.module.web.rest.endpoint

import fp.miniplan.auth.ext.globalPhoneUtil
import fp.miniplan.auth.ext.parseOrNull
import fp.miniplan.auth.ext.transactionHandleException
import fp.miniplan.auth.module.database.AuthUser
import fp.miniplan.auth.module.database.AuthUsers
import io.javalin.http.Context
import org.eclipse.jetty.http.HttpStatus
import org.jetbrains.exposed.sql.Database
import org.jetbrains.exposed.sql.SqlExpressionBuilder.eq
import org.koin.core.component.KoinComponent
import org.koin.core.component.inject

object UserMixin : Mixin<String, AuthUser?>, KoinComponent {
    private val database by inject<Database>()

    override fun handle(ctx: Context, input: String): AuthUser? {
        val parsedNumber = globalPhoneUtil.parseOrNull(input)
        if (parsedNumber == null) {
            ctx.status(HttpStatus.BAD_REQUEST_400).result("phone_number_parse_failed")
            return null
        }

        val user = database.transactionHandleException {
            AuthUser.find(AuthUsers.phoneNumber eq parsedNumber).firstOrNull()
        } ?: run {
            ctx.status(HttpStatus.BAD_REQUEST_400).result("no_such_user")
            return null
        }

        return user
    }
}
