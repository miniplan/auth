package fp.miniplan.auth.module.web

import io.javalin.http.Context
import io.javalin.websocket.WsContext
import org.eclipse.jetty.http.HttpStatus
import java.net.HttpURLConnection.HTTP_BAD_REQUEST

sealed class Response(private val text: String, private val status: Int) {
    open class WithErrorResponse(text: String, status: Int, private val errorText: String? = null) :
        Response(text, status) {
        override fun toMap() = super.toMap().apply {
            if (errorText != null) this["error"] = errorText
        }
    }

    object InvalidAuth : Response("Invalid auth", HttpStatus.BAD_REQUEST_400)

    class InvalidBody(errorText: String? = null) :
        WithErrorResponse("Invalid body data", HttpStatus.BAD_REQUEST_400, errorText)

    class InvalidParameter(errorText: String? = null) :
        WithErrorResponse("Invalid body data", HTTP_BAD_REQUEST, errorText)

    object InvalidApiVersionHeader : Response("Invalid Api version header", HttpStatus.BAD_REQUEST_400)

    class InvalidAuthentication(errorText: String? = null) :
        WithErrorResponse("Invalid authentication", HttpStatus.UNAUTHORIZED_401, errorText)

    open fun toMap(): MutableMap<String, String> {
        return mutableMapOf("response" to text)
    }

    fun send(ctx: Context) {
        ctx.status(status).json(toMap())
    }

    fun send(wsCtx: WsContext) {
        wsCtx.send(toMap())
    }
}

fun Context.send(response: Response) = response.send(this)
