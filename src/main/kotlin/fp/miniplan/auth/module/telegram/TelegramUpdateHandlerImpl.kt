package fp.miniplan.auth.module.telegram

import com.pengrad.telegrambot.TelegramBot
import com.pengrad.telegrambot.model.CallbackQuery
import com.pengrad.telegrambot.model.Message
import fe.tgbotutil.TelegramUpdateHandler
import fe.tgbotutil.command.CommandHandler
import fp.miniplan.auth.module.telegram.command.*
import org.koin.dsl.module

val telegramUpdateHandlerModule = module {
    single<TelegramUpdateHandler> {
        val bot = get<TelegramBot>()
        TelegramUpdateHandlerImpl(bot)
    }
}

class TelegramUpdateHandlerImpl(bot: TelegramBot) : TelegramUpdateHandler(
    bot, CommandHandler(listOf(CreateInvite, CreateHasuraMinistrant, DeleteUser, ListMinistranten, ListUserInvites, GetGDPRUser))
) {

    override fun handleProcessException(exception: Exception): Int? {
        exception.printStackTrace()
        return null
    }

    override fun messageCommandExecuted(message: Message, returnMsg: String?) {
    }

    override fun noCallbackHandlerFound(query: CallbackQuery) {
        println("No callback found $query")
    }
}
