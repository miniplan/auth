package fp.miniplan.auth.ext

import fp.miniplan.auth.module.database.AuthUser
import org.jetbrains.exposed.sql.Database
import org.jetbrains.exposed.sql.Transaction

@Throws(Exception::class)
fun <T> Database.transactionRaw(statement: Transaction.() -> T): T {
    return org.jetbrains.exposed.sql.transactions.transaction(this, statement)
}

fun <T> Database.transactionHandleException(printStackTrace: Boolean = false, statement: Transaction.() -> T): T? {
    return try {
        org.jetbrains.exposed.sql.transactions.transaction(this, statement)
    } catch (e: java.lang.Exception) {
        if (printStackTrace) e.printStackTrace()
        null
    }
}
