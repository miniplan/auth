package fp.miniplan.auth.module.telegram.command

import com.pengrad.telegrambot.TelegramBot
import com.pengrad.telegrambot.model.User
import fe.tgbotutil.buildMarkdown
import fe.tgbotutil.command.Command
import fe.tgbotutil.command.CommandArgumentCreator
import fe.tgbotutil.ext.sendMarkdownV2
import fp.miniplan.auth.module.hasura.HasuraConnector
import fp.miniplan.auth.module.telegram.command.base.LockedCommand
import org.koin.core.component.KoinComponent
import org.koin.core.component.inject

data class FirstNameLastName(
    val firstname: String,
    val lastname: String
) {
    companion object Creator : CommandArgumentCreator<FirstNameLastName> {
        override fun parseArgs(args: List<String>) = FirstNameLastName(args[0], args[1])
        override fun requiredArgs() = arrayOf(2)
    }
}

object CreateHasuraMinistrant : LockedCommand<FirstNameLastName>(
    "/createministrant", "<firstname> <lastname>", "Create a ministrant", FirstNameLastName.Creator
), KoinComponent {
    private val hasura by inject<HasuraConnector>()

    override fun execWithWhitelist(args: FirstNameLastName, bot: TelegramBot, user: User, chatId: Long) {
        kotlin.runCatching {
            val uuid = hasura.createMinistrant(args.firstname, args.lastname)
            bot.sendMarkdownV2(chatId, buildMarkdown {
                text("Ministrant has been created, id:").space().code(uuid.toString())
            })
        }.onFailure { it.printStackTrace() }
    }
}
