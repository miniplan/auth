package fp.miniplan.auth.init

import fe.koinhelper.ext.KoinInit
import fp.miniplan.auth.ext.transactionHandleException
import fp.miniplan.auth.module.database.AuthUsers
import fp.miniplan.auth.module.database.UserInvites
import org.jetbrains.exposed.sql.Database
import org.jetbrains.exposed.sql.SchemaUtils
import org.koin.core.component.inject


object DatabaseInit : KoinInit {
    private val database by inject<Database>()

    override fun init() {
        database.transactionHandleException {
            SchemaUtils.createMissingTablesAndColumns(
                AuthUsers, UserInvites
            )
        }
    }
}


