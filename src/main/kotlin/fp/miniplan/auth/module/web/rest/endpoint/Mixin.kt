package fp.miniplan.auth.module.web.rest.endpoint

import io.javalin.http.Context

interface Mixin<Input, Output> {
    fun handle(ctx: Context, input: Input): Output
}
