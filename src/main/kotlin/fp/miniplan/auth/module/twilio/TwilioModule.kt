package fp.miniplan.auth.module.twilio

import fp.miniplan.auth.config.TwilioConfig
import fe.httpkt.Request
import fe.httpkt.body.FormUrlEncodedBody
import org.koin.dsl.module
import java.net.HttpURLConnection
import java.util.*

val twilioModule = module {
    single {
        get<TwilioConfig>().run {
            TwilioSender(this.accountSID, this.authToken, this.serviceSID)
        }
    }
}

class TwilioSender(
    private val accountSID: String,
    private val authToken: String,
    private val serviceSID: String
) {
    private val request = Request {
        this.addHeader(
            "Authorization",
            "Basic ${Base64.getEncoder().encodeToString("$accountSID:$authToken".toByteArray())}"
        )
    }

    fun sendMessage(phoneNumber: String, message: String): HttpURLConnection {
        return request.post(
            "https://api.twilio.com/2010-04-01/Accounts/${accountSID}/Messages.json", body = FormUrlEncodedBody(
                "To" to phoneNumber, "MessagingServiceSid" to serviceSID, "Body" to message
            ), forceSend = true
        )
    }
}
