# auth

Authentication component of MiniPlan which is responsible for handling the login of users. It could be considered the most vital component in the project since it creates the sessions with which users then request content from the (Hasura) database.
