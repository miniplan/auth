package fp.miniplan.auth.module.telegram.command

import com.google.gson.Gson
import com.pengrad.telegrambot.TelegramBot
import com.pengrad.telegrambot.model.User
import fe.tgbotutil.command.CommandArgumentCreator
import fe.tgbotutil.ext.sendMessage
import fe.tgbotutil.ext.sendText
import fp.miniplan.auth.ext.transactionHandleException
import fp.miniplan.auth.module.database.AuthUser
import fp.miniplan.auth.module.hasura.HasuraConnector
import fp.miniplan.auth.module.telegram.command.base.LockedCommand
import org.jetbrains.exposed.sql.Database
import org.koin.core.component.inject
import java.util.*

data class GetGDPRUserArgument(val uuid: String) {
    companion object Creator : CommandArgumentCreator<GetGDPRUserArgument> {
        override fun parseArgs(args: List<String>) = GetGDPRUserArgument(args.single())
        override fun requiredArgs() = arrayOf(1)
    }
}

object GetGDPRUser : LockedCommand<GetGDPRUserArgument>(
    "/gdpr",
    "<user_id>",
    "Get GDPR archive for given user id",
    GetGDPRUserArgument.Creator
) {
    private val database by inject<Database>()
    private val hasura by inject<HasuraConnector>()

    override fun execWithWhitelist(args: GetGDPRUserArgument, bot: TelegramBot, user: User, chatId: Long) {
        val user = database.transactionHandleException {
            AuthUser.findById(UUID.fromString(args.uuid))
        }

        if (user == null) {
            bot.sendText(chatId, "Failed to get user by id")
            return
        }

        val ministrant = hasura.getUserMinistrant(args.uuid)

        val map = mutableMapOf(
            "userId" to user.userId.value,
            "phoneNumber" to user.phoneNumber,
            "accountState" to user.state,
            "firstname" to user.firstname,
            "lastname" to user.lastname,
            "ministrant" to ministrant,
            "selectedMinistranten" to hasura.getUserMinistranten(args.uuid).map { it.second },
        )

        if(ministrant != null){
            map["tasks"] = hasura.getMinistrantTasks(ministrant)
        }

        bot.sendText(chatId, Gson().toJson(map))
    }
}
