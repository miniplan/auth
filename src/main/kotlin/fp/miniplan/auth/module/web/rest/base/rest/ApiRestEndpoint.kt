package fp.miniplan.auth.module.web.rest.base.rest


import fe.javalin.rest.ApiEndpoint
import fp.miniplan.auth.module.web.Response
import io.javalin.http.Context
import io.javalin.http.HandlerType
import io.javalin.security.RouteRole
import io.javalin.validation.BodyValidator

abstract class ApiRestEndpoint<T>(
    private val classes: Map<RestApiVersion, Class<out T>>,
    vararg path: String,
    roles: Array<out RouteRole>,
    method: HandlerType
) : ApiEndpoint(*path, roles = roles, method = method) {

    companion object {
        const val REST_API_VERSION_NAME = "Rest-Api-Version"
    }

    override fun handle(ctx: Context) {
        val clazz = classes[ctx.apiVersion()] ?: run {
            Response.InvalidApiVersionHeader.send(ctx)
            return
        }

        val body = ctx.bodyValidator(clazz).getOrNull() ?: run {
            Response.InvalidBody().send(ctx)
            return
        }

        this.handle(ctx, body)
    }

    abstract fun handle(ctx: Context, body: T)
}

fun Context.apiVersion() = RestApiVersion.find(this.header(ApiRestEndpoint.REST_API_VERSION_NAME))
    ?: RestApiVersion.latest()


fun <T> BodyValidator<T>.getOrNull(): T? {
    //this used to be a thing in Javalin 3.x.x, but is not anymore for some bizarre reason
    return try {
        this.get()
    } catch (e: Exception) {
        null
    }
}


enum class RestApiVersion {
    V1;

    companion object {
        fun find(version: String?) = values().find {
            it.name.equals(version, ignoreCase = true)
        }

        fun latest() = values().last()
    }
}
