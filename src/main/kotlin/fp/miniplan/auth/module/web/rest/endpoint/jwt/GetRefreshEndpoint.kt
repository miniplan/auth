package fp.miniplan.auth.module.web.rest.endpoint.jwt

import com.auth0.jwt.exceptions.AlgorithmMismatchException
import com.auth0.jwt.exceptions.InvalidClaimException
import com.auth0.jwt.exceptions.SignatureVerificationException
import com.auth0.jwt.exceptions.TokenExpiredException
import fe.javalin.rest.ApiEndpoint
import fp.miniplan.auth.ext.set
import fp.miniplan.auth.module.web.JwtHandler
import fp.miniplan.auth.module.web.WebRole
import fp.miniplan.auth.module.web.rest.endpoint.TokenState
import io.javalin.http.Context
import io.javalin.http.HandlerType
import org.eclipse.jetty.http.HttpStatus
import org.jetbrains.exposed.sql.Database
import org.koin.core.component.KoinComponent
import org.koin.core.component.inject
import redis.clients.jedis.JedisPooled

object GetRefreshEndpoint : ApiEndpoint("refresh", roles = arrayOf(WebRole.Default), method = HandlerType.GET),
    KoinComponent {
    private val sqlDatabase by inject<Database>()

    private val jwtHandler by inject<JwtHandler>()
    private val redis by inject<JedisPooled>()

    private val refreshExceptions = mapOf(
        AlgorithmMismatchException::class to "Invalid signing algorithm!",
        SignatureVerificationException::class to "Invalid signature!",
        TokenExpiredException::class to "Token has already expired!",
        InvalidClaimException::class to "Invalid token claims"
    )

    override fun handle(ctx: Context) {
        val authHeader = ctx.header("Authorization")
        if (authHeader == null) {
            ctx.status(HttpStatus.BAD_REQUEST_400).result("No Authorization header present!")
            return
        }

        val split = authHeader.split("Bearer ")
        if (split.size != 2) {
            ctx.status(HttpStatus.BAD_REQUEST_400).result("Authorization header is invalid!")
            return
        }

        val refreshToken = try {
            jwtHandler.verify(jwtHandler.refreshType, split[1])
        } catch (e: Exception) {
            ctx.status(HttpStatus.UNAUTHORIZED_401).result(refreshExceptions[e::class] ?: "Jwt error!")
            return
        }

//        val rt1Value = redis.get(refreshToken.token)
//        if (rt1Value == null) {
//            ctx.status(HttpStatus.INTERNAL_SERVER_ERROR_500)
//                .result("Refresh token couldn't be found, please login again!")
//            return
//        }

//        if (rt1Value == TokenState.Invalidated.name) {
//            ctx.status(HttpStatus.UNAUTHORIZED_401)
//                .result("Token reuse detected, please login again!")
//            return
//        }

//        if (rt1Value != TokenState.Generated.name) {
//            redis.set(rt1Value, TokenState.Invalidated.name) {
//                this.keepttl()
//            }
//
//            ctx.status(HttpStatus.UNAUTHORIZED_401).result("Token reuse detected, please login again!")
//            return
//        }

        val (appName, userId) = try {
            jwtHandler.extractToken(refreshToken)
        } catch (e: JwtHandler.MissingClaimException) {
            ctx.status(HttpStatus.UNAUTHORIZED_401).result(e.message ?: "Jwt claims are missing!")
            return
        }

//        val updateCounterValue = redis.get(jwtHandler.redisUpdateCounterKey.format(userId, appName))?.toLongOrNull()
//        if(updateCounterValue != null && updateCounterValue > updateCounter){
//            ctx.status(HttpStatus.UNAUTHORIZED_401).result("Your access has been updated, please login again!")
//            return
//        }

        val (newAccessToken, newRefreshToken) = jwtHandler.createBothTokens(userId, appName)

//        if (rt1Value == TokenState.Generated.name) {
//            redis.set(refreshToken.token, newRefreshToken) {
//                exAt(refreshToken.getClaim("exp").asLong())
//            }
//
//            redis.set(newRefreshToken, TokenState.Generated.name) {
//                ex(jwtHandler.getTokenExpiry(jwtHandler.refreshType))
//            }
//        }

        ctx.json(NewTokensResponse(newAccessToken, newRefreshToken))
    }
}

data class NewTokensResponse(
    val newAccessToken: String,
    val newRefreshToken: String,
)
